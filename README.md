Live demo: https://fir-touch-actions.firebaseapp.com

Open on touchscreen device and on any other device. Drag box on touchscreen device and see it update on other device.

Install:

Create project at https://firebase.google.com/

Set database rules to public (You would want to add authenticaton to real-world app)

git clone https://gitlab.com/gabe4937/firebase-touch-actions

npm install -g firebase-tools 

firebase login

firebase use --add 

select the Firebase project you have created.

firebase serve (serve on localhost)

firebase deploy (deploy on Firebase Project hosting)